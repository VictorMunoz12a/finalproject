package com.softtek.javaweb.conexion;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Conexion {
	@Bean
	//@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = 
				new DriverManagerDataSource();
		// MySQL database we are using
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/momentum?serverTimezone=UTC#");// change url
		dataSource.setUsername("root");// change userid
		dataSource.setPassword("1234");// change pwd

		return dataSource;
	}
}
