package com.softtek.javaweb.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Registros {
	@Id
	int registroId;
	String name;
	String dateTime;
	String checkInOut;
	
	public Registros() {

		// TODO Auto-generated constructor stub
	}
	public int getRegistroId() {
		return registroId;
	}
	public void setRegistroId(int registroId) {
		this.registroId = registroId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getCheckInOut() {
		return checkInOut;
	}
	public void setCheckInOut(String checkInOut) {
		this.checkInOut = checkInOut;
	}	
}