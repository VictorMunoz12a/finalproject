package com.softtek.javaweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.softtek.javaweb.model.Person;

@Repository("PersonRepository")
public interface PersonRepository extends JpaRepository<Person,Integer> {

}
