package com.softtek.javaweb.repository;

import java.util.List;

import com.softtek.javaweb.model.Registros;

public interface RegisterRepository {
List<Registros> getAllRegistros();
Registros createRegistros(Registros registro);
}
