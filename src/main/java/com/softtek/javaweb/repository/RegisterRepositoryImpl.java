package com.softtek.javaweb.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.javaweb.model.Registros;

@Repository("RegistroRepository")
public class RegisterRepositoryImpl  implements RegisterRepository{

	@PersistenceContext
	private EntityManager entityManager;
	@Override
	public List<Registros> getAllRegistros() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Registros createRegistros(Registros registro) {
		entityManager.persist(registro);
		return null;
	}

}
