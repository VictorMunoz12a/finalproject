package com.softtek.javaweb.service;

import com.softtek.javaweb.model.Person;

public interface PersonService {
Person createPerson (long personId, String personName);
}
