package com.softtek.javaweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.softtek.javaweb.model.Person;
import com.softtek.javaweb.repository.PersonRepository;

public class PersonServiceImpl implements PersonService{

	@Autowired
	@Qualifier("PersonRepository")
	private PersonRepository personRepository;
	@Override
	public Person createPerson(long personId, String personName) {
		// TODO Auto-generated method stub
		System.out.println("In service");
		Person p= new Person();
		p.setPersonId(personId);
		p.setName(personName);
		return personRepository.save(p);
	}
}
