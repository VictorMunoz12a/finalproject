package com.softtek.javaweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.softtek.javaweb.model.Registros;
import com.softtek.javaweb.repository.RegisterRepository;

public class RegistroServiceImpl implements UploadService {

	@Autowired
	@Qualifier("RegistroRepository")
	private RegisterRepository registerRepository;

	@Override
	public Registros createRegistros(Registros registro) {
		// TODO Auto-generated method stub
		return registerRepository.createRegistros(registro);
	}

	@Override
	public List<Registros> getAllRegistros() {
		// TODO Auto-generated method stub

		return null;
	}
}
