package com.softtek.javaweb.service;

import java.util.List;

import com.softtek.javaweb.model.Registros;

public interface UploadService {
List<Registros> getAllRegistros();
Registros createRegistros(Registros registro);
}
